const express = require("express");
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var port = process.env.PORT

app.get('/', function (req, res) {
    respuesta = {
        codigo: 200,
        mensaje: 'Punto de inicio'
    };
    res.send(respuesta);
});
app.post('/mutation', function (req, res) {


    let mutacionhorizontalcount = 0;
    let mutacionverticalcount = 0;
    let mutaciondiagonalcount = 0;
    let mutaciondiagonalinvertidacount = 0;

    function parametroinvalido(param) {
        var regex = /[^ATCG]/;
        if (!regex.test(param)) {
            return true;
        }
        return false;
    }

    function mutacionhorizontal(param) {
        var regexc = /C{4}/;
        var regexa = /A{4}/;
        var regext = /T{4}/;
        var regexg = /G{4}/;
        if (regexc.test(param) || regexa.test(param) || regext.test(param) || regexg.test(param)) {
            return true;
        }
        return false;
    }

    var countvertical = 0;
    function validavertical() {
        adn.forEach(function (element, key) {
            for (i = 0; i < element.length; i++) {
                let letra = element.substring(i, i + 1);
                mutacionvertical(key, letra, i);
            }

        });
    }
    function mutacionvertical(key, letra, index) {
        if (key >= adn.length) {
            if (countvertical === 4) {
                mutacionverticalcount++;
                if (mutacionverticalcount === 2) {
                    mutacionencontrada();
                }
            } else {
                countvertical = 0;
            }
            return false;
        }
        var n = adn[key].indexOf(letra, index);
        if (n === index) {
            countvertical++;
            mutacionvertical(key + 1, letra, index)
        } else {
            if (countvertical === 4) {
                mutacionverticalcount++;
                if (mutacionverticalcount === 2) {
                    mutacionencontrada();
                }
            } else {
                countvertical = 0;
            }
        }


    }

    var count = 0;
    function validaDiagonal() {
        adn.forEach(function (element, key) {
            for (i = 0; i < element.length; i++) {
                let letra = element.substring(i, i + 1);
                mutaciondiagonal(key, letra, i);
            }

        });
    }
    function mutaciondiagonal(key, letra, index) {
        if (key >= adn.length) {
            if (count === 4) {
                count = 0;
                mutaciondiagonalcount++;
                if (mutaciondiagonalcount === 2) {
                    mutacionencontrada();
                }
            } else {
                count = 0;
            }
            return false;
        }
        var n = adn[key].indexOf(letra, index);
        if (n === index) {
            count++;
            mutaciondiagonal(key + 1, letra, index + 1)
        } else {

            if (count === 4) {
                count = 0;
                mutaciondiagonalcount++;
                if (mutaciondiagonalcount === 2) {
                    mutacionencontrada();
                }
            } else {
                count = 0;
            }

        }


    }


    var countinvertida = 0;
    function validaDiagonalInvertida() {
        adn.forEach(function (element, key) {
            for (i = element.length - 1; i >= 0; i--) {

                let letra = element.substring(i, i + 1);
                mutaciondiagonalinvertida(key, letra, i);
            }

        });
    }
    function mutaciondiagonalinvertida(key, letra, index) {
        if (key >= adn.length) {
            if (countinvertida === 4) {
                countinvertida = 0;
                mutaciondiagonalinvertidacount++;
                if (mutaciondiagonalinvertidacount === 2) {
                    mutacionencontrada();
                }
            } else {
                countinvertida = 0;
            }
            return false;
        }
        var n = adn[key].indexOf(letra, index);
        if (n === index) {
            countinvertida++;
            mutaciondiagonalinvertida(key + 1, letra, index - 1);
        } else {

            if (countinvertida === 4) {
                countinvertida = 0;
                mutaciondiagonalinvertidacount++;
                if (mutaciondiagonalinvertidacount === 2) {
                    mutacionencontrada();
                }
            } else {
                countinvertida = 0;
            }

        }

    }


    function mutacionencontrada(params) {
        respuesta = {
            error: false,
            codigo: 200,
            mensaje: 'Con mutación'
        };
        res.status(200).send(respuesta);
    }
    if (!req.body.dna) {
        respuesta = {
            error: true,
            codigo: 502,
            mensaje: 'Falta el parametro dna'
        };
    } else {
        var adn = req.body.dna;
        let adnlength = adn.length;

        adn.forEach(function (element, key) {
            if (element.length !== adnlength || !parametroinvalido(element)) {
                respuesta = {
                    error: true,
                    codigo: 503,
                    mensaje: 'Bases nitrogenada NO válida'
                };
                res.send(respuesta);
            }

            //buscar mutacion horizontalmente
            if (mutacionhorizontal(element)) {

                mutacionhorizontalcount++;
            }
            if (mutacionhorizontalcount === 2) {
                mutacionencontrada();
            }

        });
        validavertical();
        validaDiagonal();
        validaDiagonalInvertida();

        let totalmutaciones = mutacionverticalcount + mutaciondiagonalcount + mutacionhorizontalcount + mutaciondiagonalinvertidacount;

        if (totalmutaciones > 1) {
            mutacionencontrada();
        }

        if (Array.isArray(adn)) {

            respuesta = {
                error: true,
                codigo: 403,
                data: adn,
                mensaje: 'Forbidden'
            };
            res.status(403).send(403);
        }
    }

    res.send(respuesta);
});
//app.listen(port);
app.listen(3001, () => {
    console.log("El servidor está inicializado en el puerto 3001");
});