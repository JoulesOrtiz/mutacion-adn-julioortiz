# mutacion-adn-julioortiz

Este proyecto es el resultado de un ejercicio solicitado para detectar la mutación en el ADN de una persona.
Para eso es necesario crear un servicio con un método o función con la siguiente firma (En JavaScript/Node JS):

## SquintFront

Proyecto generado con 
nodejs v12.17.0
npm 6.14.4

## Instrucciones de instalacion. 
Para la instalación y ejecución del proyecto.

```
git clone https://gitlab.com/JoulesOrtiz/mutacion-adn-julioortiz.git
cd mutacion-adn-julioortiz
npm install
node index.js
```   
*** NOTA: 
    Despues de ejecutar 'node index.js' puedes verificar mediante un navegador web con la url 
http://localhost:3001/ esta desplegara  '{"codigo":200,"mensaje":"Punto de inicio"}' ***

## Algunos casos de uso 

 El servicio “http://localhost:3001/mutation/” puede detectar si existe mutación enviando la secuencia de ADN mediante un HTTP POST con un JSON el cual tenga el

siguiente formato:
POST → /mutation/
{
"dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
}
En caso de verificar una mutación, debería devolver un HTTP 200-OK, en caso contrario un 403-Forbidden

Sin mutación:
{
    "dna" : 
       ["ATGCGA",
        "CAGTGC",
        "TTATTT",
        "AGACGG",
        "GCGTCA",
        "TCACTG"]
}


Con mutación:
{
    "dna" : 
       ["ATGCGA",
        "CAGTGC",
        "TTATGT",
        "AGAAGG",
        "CCCCTA",
        "TCACTG"]
}

